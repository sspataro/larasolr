<?php
 
return [
    "country" => "addressCountry",
    "City" => "addressLocality",
    "region" => "addressRegion", 
    "postalCode" => "postalCode" ,
    "locationAddress" => "streetAddress"
];
 