<?php
/*  
"@context":'https:\\schema.org",
"@type":"Organization",
*/                
return [
    "id" => "identifier",
    "description" => "description",
    "duration" => "duration",  
    "startDate"  => "startDate" , 
    "endDate" => "endDate",
    "title" => "name"
];

/*
{  
      "@context":"https:\/\/schema.org",
      "@type":"Event",
    -  "description":"4th Platoon Houston will partner with Recipe for Success addressing childhood obesity and food deserts in the Houston area by focusing on community farming. 4th platoon members will learn how to build gardens and will apply what they have learned in communities that do not have access to fresh produce.",
      "director":{  
         "@type":"Person",
         "email":"daviddenboer@live.com",
         "image":"https:\/\/s3-us-west-2.amazonaws.com\/changemaker-prod\/users\/images\/uajr3KOl9Lpyqcmcku8XEyAMAVJ1VWZkW4kIEoLN\/avatar\/David+den+Boer+photo.JPG",
         "name":"David den Boer"
      },
    -  "duration":"PT4H30M",
    -  "endDate":"2017-01-16T13:00:00-06:00",
      "identifier":"https:\/\/staging.changemaker.pointsoflight.org\/api\/websub\/publications\/events\/eD843Ql5",
      "location":{  
         "@type":"Place",
         "address":{  
            "@type":"PostalAddress",
            "addressCountry":"US",
            "addressLocality":"Houston",
            "addressRegion":"Texas",
            "postalCode":"77051",
            "streetAddress":"10401 Scott Street"
         },
       -  "geo":{  
            "@type":"GeoCoordinates",
            "latitude":"29.6466808",
            "longitude":"-95.3679013"
         }
      },
      "name":"MLK Day Service Project with Mission Continues in Houston, Texas",
      "organizer":{  
         "@type":"Organization",
         "identifier":"https:\/\/staging.changemaker.pointsoflight.org\/api\/websub\/publications\/organizations\/volunteer-houston"
      },
      "startDate":"2017-01-16T08:30:00-06:00"
   }

   */