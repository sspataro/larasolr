<?php
/*
"location":{  
"@type":"Place",
"address":{  
    "@type":"PostalAddress",
    "addressCountry":".$this->encodeOutput($document,"BillingCountry").",
    "addressLocality":".$this->encodeOutput($document,"BillingCity").",
    "addressRegion":".$this->encodeOutput($document,"BillingState")." ,
    "postalCode":".$this->encodeOutput($document,"BillingPostalCode").",
    "streetAddress":".$this->encodeOutput($document,"BillingStreet")." 
    }
},
*/
     
return [
    "BillingCountry" => "addressCountry",
    "BillingCity" => "addressLocality",
    "BillingState" => "addressRegion", 
    "BillingPostalCode" => "postalCode" ,
    "BillingStreet" => "streetAddress"
];
 