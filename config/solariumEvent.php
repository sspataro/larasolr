<?php

return [
    'endpoint' => [
        'server' => [
            'host' => env('SOLR_HOST', 'solr-stg.handsonconnect.org'),
            'port' => env('SOLR_PORT', '8983'),
            'path' => env('SOLR_PATH', '/solr/'),
            'core' => env('SOLR_CORE', 'pen')
        ]
    ]
];