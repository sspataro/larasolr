<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Config;
use Solarium\Client;
use Response;
 

class OrganizationController extends Controller
{

    protected $client;
    protected $orgMap;
    protected $placeMap;
    protected $urlPre;

    public function __construct(\Solarium\Client $client)
    {
        $this->client=new Client(Config::get('solariumOrg'));
        $this->orgMap=Config::get('orgMap');
        $this->placeMap=Config::get('placeMap');
        $this->urlPre=env('APP_URL'). '/api/websub/publications/';
    }

    // 
    public function ping()
    {
        // create a ping query

        dd($this->placeMap);

        $ping = $this->client->createPing();

        // execute the ping query
        try {
            $this->client->ping($ping);
            return response()->json('OK');
        } catch (\Solarium\Exception $e) {
            return response()->json('ERROR', 500);
        }
    }

    public function test()
    {
        dd($this->client);
    }


    public function getOrgs($start,$rows){
        $client=$this->client;
        $query = $client->createSelect();
        //$query->setStart($offset)->setRows($limit);
        //dd("$start ---- $rows");
        $query->setStart($start);
        $query->setRows($rows);
   //    $facets = $query->getFacetSet();
        $resulset = $client->select($query);
 
        $response=$resulset->getResponse();
        $response->getStatusCode();
        $body=json_decode($response->getBody());
     
        
        $response = array(
         "numFound"=>$body->response->numFound,
         "docs"=>$this->encodeResutls($body->response->docs)
        );

        return $response;

       
    }

    public function getOrgsPaged($page=1,$perPage=10){  
            if ($page==1){
                $start=0;
            }else{
                $start=$page*$perPage;
            }

            $offset=($page)*$perPage;
            $response=$this->getOrgs($start,$perPage);
            $collection=$response['docs'];
            $total=$response['numFound'];

  
            $last=intdiv($total,$perPage)-1;
            
  
            $link='Link: <' . $this->urlPre. 'organizations/1/'. $perPage . '>; rel="first",' ;
            if ($page!=1){ //first page doesn't have previous
                $prevPage=$page-1;
                $link=$link.'<'.$this->urlPre. 'organizations/'. $prevPage  . '/'. $perPage . '>; rel="prev",';
            }   
            
            if ($page!=$last){ //last page doesn't have next
                $nextPage=$page+1;
                $link=$link.'<' . $this->urlPre. 'organizations/'. $nextPage . '/'. $perPage . '>; rel="next",'; 
            }
 
            $link=$link.'<' . $this->urlPre. 'organizations/'. $last . '/'. $perPage . '>; rel="last"';
  
 
            return response($response['docs'])->header('Link',$link);
            

            //return $response['docs'];
           
            // $paginate = new LengthAwarePaginator($collection, $total, $perPage, $page, ['path'=>url('api/organizations')]);
            // return $paginate->withPath('organizations');;
    }

    public function getOrgById($id){

        $client=$this->client;
        $query = $client->createSelect();
        $query->addFilterQuery(array('key'=>'id', 'query'=>$id, 'tag'=>'include'));
        //$facets = $query->getFacetSet();
        $resulset = $client->select($query);
        $response=$resulset->getResponse();
        $response->getStatusCode();
        $body=json_decode($response->getBody());
        $docs=$body->response->docs;
        return $this->encodeResutls($docs);

    }

    public function encodeResutls($docs){
        $rVal=[];
        foreach ($docs as $document) {
            array_push($rVal,$this->buildOrg($document));
        }
        return $rVal;

    }

    function buildOrg($document){
        $rVal = [
            "@context" => "https:\\schema.org",
            "@type"=>"Organization"
        ];
        $rVal=$this->extractMappedInfo($document,$this->orgMap,$rVal);
        $rVal["location"]=$this->buildPlace($document);
        $rVal["identifier"]=$this->urlPre . 'organization/' . $rVal["identifier"];

        return $rVal;
    }

    function buildPlace($document){
        $rVal = [
            "@type"=>"Place"
        ];

        $place=["@type"=>"PostalAddress"];
        $place=$this->extractMappedInfo($document,$this->placeMap,$place);
        $rVal["address"]=$place;
    
        return $rVal;


    }

    function extractMappedInfo($document,$map,$inputArray){
        $rVal = $inputArray;
        foreach ($document as $field => $value) {
            // this converts multivalue fields to a comma-separated string
            if (is_array($value)) {
                $value = implode(', ', $value);
            }  

            if (array_key_exists ($field,$map)){
                $rVal[$map[$field]]=$value;
            }   
        }
        return $rVal;
    }
 


}
