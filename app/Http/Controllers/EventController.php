<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Solarium\Client;
use Illuminate\Pagination\LengthAwarePaginator; 
use Response;
 

class EventController extends Controller
{

    protected $client;
    protected $orgMap;
    protected $placeMap;
    protected $eventMap;
    protected $geoMap;
    protected $urlPre;

    public function __construct(\Solarium\Client $client)
    {
        $this->client=new Client(Config::get('solariumEvent'));
        $this->orgMap=Config::get('orgEventMap');
        $this->placeMap=Config::get('placeEventMap');
        $this->eventMap=Config::get('eventMap');   
        $this->geoMap=Config::get('geoEventMap');    
        $this->urlPre=env('APP_URL'). '/api/websub/publications/'; 
    }

    // 
    public function ping()
    {
        // create a ping query
        $ping = $this->client->createPing();

        // execute the ping query
        try {
            $this->client->ping($ping);
            return response()->json('OK');
        } catch (\Solarium\Exception $e) {
            return response()->json('ERROR', 500);
        }
    }

    public function test()
    {
        dd($this->client);
    }

    public function getEventById($id){
        $client=$this->client;
        $query = $client->createSelect();
        $query->createFilterQuery('id')->setQuery('id:'.$id);
        
        $resultset = $client->select($query);
      
        $response=$resultset->getResponse();
        $response->getStatusCode();
        $body=json_decode($response->getBody());
        $docs=$body->response->docs;

        return $docs;

    }

    public function getEvents($start,$rows){
        $client=$this->client;
        $query = $client->createSelect();
        $query->setStart($start);
        $query->setRows($rows);
        $resulset = $client->select($query);
 
        $response=$resulset->getResponse();
        $response->getStatusCode();
        $body=json_decode($response->getBody());
     
        
        $response = array(
         "numFound"=>$body->response->numFound,
         "docs"=>$this->encodeResutls($body->response->docs)
        );

        return $response;

         

}

    public function getEventsPaged($page=1,$perPage=50){
        if ($page==1){
            $start=0;
        }else{
            $start=$page*$perPage;
        }

        $offset=($page)*$perPage;
        $response=$this->getEvents($start,$perPage);
        $collection=$response['docs'];
        $total=$response['numFound'];


        $last=intdiv($total,$perPage)-1;
        

        $link='Link: <' . $this->urlPre. 'events/1/'. $perPage . '>; rel="first",' ;
        if ($page!=1){ //first page doesn't have previous
            $prevPage=$page-1;
            $link=$link.'<'.$this->urlPre. 'events/'. $prevPage  . '/'. $perPage . '>; rel="prev",';
        }   
        
        if ($page!=$last){ //last page doesn't have next
            $nextPage=$page+1;
            $link=$link.'<' . $this->urlPre. 'events/'. $nextPage . '/'. $perPage . '>; rel="next",'; 
        }

        $link=$link.'<' . $this->urlPre. 'events/'. $last . '/'. $perPage . '>; rel="last"';


        return response($response['docs'])->header('Link',$link);
    }


    public function encodeResutls($docs){
        $rVal=[];
        foreach ($docs as $document) {
            array_push($rVal,$this->buildEvent($document));
        }
        return $rVal;

    }

    function buildEvent($document){
        $rVal = [
            "@context" => "https:\\schema.org",
            "@type"=>"Event"
        ];
        $rVal=$this->extractMappedInfo($document,$this->eventMap,$rVal);
        $rVal["location"]=$this->buildPlace($document);
        $rVal["organizer"]=$this->buildOrg($document);
        $rVal["geo"]=$this->buildGeo($document);
        $rVal["identifier"]=env('APP_URL'). '/api/websub/publications/event/' . $rVal["identifier"];
  

        return $rVal;        
    }

    function buildGeo($document){
        $rVal = [
            "@type"=>"GeoCoordinates"
        ];    
        return $this->extractMappedInfo($document,$this->geoMap,$rVal);
        
    }

    function buildPlace($document){
        $rVal = [
            "@type"=>"Place"
        ];

        $place=["@type"=>"PostalAddress"];
        $place=$this->extractMappedInfo($document,$this->placeMap,$place);
        $rVal["address"]=$place;
    
        return $rVal;


    }

    function buildOrg($document){
        $rVal = [
            "@type"=>"Organization"
        ];
        $rVal=$this->extractMappedInfo($document,$this->orgMap,$rVal);
        return $rVal;
    }

    function extractMappedInfo($document,$map,$inputArray){
        $rVal = $inputArray;
        foreach ($document as $field => $value) {
            // this converts multivalue fields to a comma-separated string
            if (is_array($value)) {
                $value = implode(', ', $value);
            }  

            if (array_key_exists ($field,$map)){
                $rVal[$map[$field]]=$value;
            }   
        }
        return $rVal;
    }

}
