<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Config;
use Solarium\Client;

class OrganizationServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Register any application services.
     *
     * @return  void
     */
    public function register()
    {
        $this->app->bind(Client::class, function ($app) {
            return new Client($app['config']['solarium']);
        });
    }

    public function provides()
    {
        return [Client::class];
    }
}
