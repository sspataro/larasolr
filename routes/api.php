<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('websub/publications/pingOrg', 'OrganizationController@ping');

Route::get('websub/publications/organizations/{start?}/{pageSize?}', 'OrganizationController@getOrgsPaged');

Route::get('websub/publications/organization/{id}', 'OrganizationController@getOrgById');

Route::get('websub/publications/pingEvent', 'EventController@ping');

Route::get('websub/publications/events/{start?}/{pageSize?}', 'EventController@getEventsPaged');

Route::get('websub/publications/event/{id}', 'EventController@getEventById');




